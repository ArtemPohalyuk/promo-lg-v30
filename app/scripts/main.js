// find polyfill
if ( !Array.prototype.find ) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);
      var len = o.length >>> 0;

      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      var thisArg = arguments[1];
      var k = 0;

      while (k < len) {
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }

        k++;
      }

      return undefined;
    }
  });
}

// assign polyfill
if ( typeof Object.assign != 'function' ) {
  Object.assign = function(target, varArgs) {
    'use strict';
    if (target == null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
      var nextSource = arguments[index];

      if (nextSource != null) {
        for (var nextKey in nextSource) {
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  };
}

// location origin polyfill
if ( !window.location.origin ) {
  window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
}

// detect IE
function detectIE() {
  var ua = window.navigator.userAgent;

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  return false;
}

// BasketState
const BasketState = function () {
  this.items = [];
};

BasketState.prototype.getItems = function() {
  return this.items;
};

BasketState.prototype.findItem = function( id ) {
  return this.items.find( ( item ) => {
    return item.id === id;
  });
};

BasketState.prototype.setItem = function( id, isGift = false ) {
  if ( !id ) {
    return false;
  }

  const item = this.findItem( id );

  if ( !item ) {
    this.items.push({
      id: id,
      count: 1,
      isGift: isGift
    });
  }
};

BasketState.prototype.removeItem = function( id ) {
  const item = this.findItem( id );

  if ( item ) {
    let itemIndex = null;
    this.items.forEach( function( item, index ) {
      if ( item.id === id ) {
        itemIndex = index;
      }
    });

    this.items.splice( itemIndex, 1 );
  }
};

BasketState.prototype.setCount = function( id, count ) {
  const item = this.findItem( id );

  if ( item ) {
    item.count = count;
  }
};

BasketState.prototype.setType = function( id, type ) {
  const item = this.findItem( id );

  if ( item && type ) {
    item.type = type;
  }
};

$( 'document' ).ready( function() {
  // ie label
  (function() {
    let version = detectIE();

    if ( version && detectIE() < 12 ) {
      $( 'body' ).prepend( '<p class="browserupgrade">Функция оплаты недоступна для браузера <strong>Internet Explorer</strong>. Пожалуйста воспользуйтесь <a href="http://browsehappy.com/" target="blank">альтернативным браузером</a>.</p>' );
    }
  })();

  // state of basket
  const locationOrigin = location.origin;
  const basketState = new BasketState();

  // fancybox
  (function() {
    $( '[data-fancybox]' ).fancybox({
      loop: true
    });
  })();

  // masked
  (function() {
    $( 'input[type="tel"]' ).mask( '+7-999-999-99-99' );
  })();

  // collapse
  (function() {
    $( '.js-collapse-trigger' ).on( 'click', function( e ) {
      e.preventDefault();

      const $this = $( this );

      if ( $this.hasClass( 'during' ) ) {
        return false;
      }

      const $parent = $this.closest( '.js-collapse' );
      const $content = $parent.find( '.js-collapse-content' );
      const contentHeight = $content.outerHeight();

      if ( $this.hasClass( 'active' ) ) {
        $content.css( 'max-height', contentHeight + 'px' );

        setTimeout( function() {
          $this.addClass( 'during' );
          $content.css({
            'max-height': '0',
            'transition-duration': contentHeight / 2000 + 's'
          });
        }, 25 );

        setTimeout( function() {
          $this.removeClass( 'during' );
          $content.css({
            'display': 'none',
            'max-height': 'none'
          });
        }, contentHeight / 2 );
      } else {
        $content.css({
          'display': 'block',
          'max-height': '0'
        });

        setTimeout( function() {
          $this.addClass( 'during' );
          $content.css({
            'max-height': contentHeight + 'px',
            'transition-duration': contentHeight / 2000 + 's'
          });
        }, 25 );

        setTimeout( function() {
          $this.removeClass( 'during' );
          $content.css( 'max-height', 'none' );
        }, contentHeight / 2 );
      }

      $this.toggleClass( 'active' );
    });
  })();

  // scrollTo
  (function() {
    $.fn.scrollTo = function( to ) {
      $( 'html, body' ).stop().animate({
        'scrollTop': to
      }, 500, 'swing' );
    };

    $( '.js-scroll-to' ).on( 'click', function( e ) {
      e.preventDefault();

      let $this = $( this );
      let target = $this.attr( 'data-target' );
      let $target = $( target );
      let to = $target.offset().top;

      $().scrollTo( to );
    });
  })();

  // choose device
  (function() {
    const itemsData = [
      {
        'id': '3',
        'title': 'LG V30+, 128 ГБ, черный',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, черный',
        'imageUrl': './images/goods/3.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '101',
          'title': 'Пылесос LG CordZero',
          'titleHtml': 'Пылесос LG CordZero',
          'imageUrl': './images/goods/1.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '4',
        'title': 'LG V30+, 128 ГБ, синий',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, синий',
        'imageUrl': './images/goods/4.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '102',
          'title': 'Пылесос LG CordZero',
          'titleHtml': 'Пылесос LG CordZero',
          'imageUrl': './images/goods/1.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '5',
        'title': 'LG V30+, 128 ГБ, фиолетовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, фиолетовый',
        'imageUrl': './images/goods/5.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '103',
          'title': 'Пылесос LG CordZero',
          'titleHtml': 'Пылесос LG CordZero',
          'imageUrl': './images/goods/1.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '6',
        'title': 'LG V30+, 128 ГБ, розовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, розовый',
        'imageUrl': './images/goods/6.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '104',
          'title': 'Пылесос LG CordZero',
          'titleHtml': 'Пылесос LG CordZero',
          'imageUrl': './images/goods/1.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '7',
        'title': 'LG G6, черный',
        'titleHtml': 'LG G6, черный',
        'imageUrl': './images/goods/7.png',
        'model': 'H870S',
        'price': '39990',
        'priceHtml': '39 990 руб.',
        'type': 'device',
        'gift': {
          'id': '37',
          'title': 'Набор косметики к LG G6',
          'titleHtml': 'Набор косметики к LG G6',
          'imageUrl': './images/goods/37.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '8',
        'title': 'LG Q6+, черный',
        'titleHtml': 'LG Q6<sup class=\'basket-table-description__name-sup\'>+</sup>, черный',
        'imageUrl': './images/goods/8.png',
        'model': 'M700DSN',
        'price': '19990',
        'priceHtml': '19 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3801',
          'title': 'Набор косметики к LG Q6+',
          'titleHtml': 'Набор косметики к LG Q6+',
          'imageUrl': './images/goods/38.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '9',
        'title': 'LG Q6+, синий',
        'titleHtml': 'LG Q6<sup class=\'basket-table-description__name-sup\'>+</sup>, синий',
        'imageUrl': './images/goods/9.png',
        'model': 'M700DSN',
        'price': '19990',
        'priceHtml': '19 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3802',
          'title': 'Набор косметики к LG Q6+',
          'titleHtml': 'Набор косметики к LG Q6+',
          'imageUrl': './images/goods/38.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '10',
        'title': 'LG Q6, черный',
        'titleHtml': 'LG Q6, черный',
        'imageUrl': './images/goods/10.png',
        'model': 'M700 AN',
        'price': '17990',
        'priceHtml': '17 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3901',
          'title': 'Набор косметики к LG Q6',
          'titleHtml': 'Набор косметики к LG Q6',
          'imageUrl': './images/goods/39.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '11',
        'title': 'LG Q6, золотой',
        'titleHtml': 'LG Q6, золотой',
        'imageUrl': './images/goods/11.png',
        'model': 'M700 AN',
        'price': '17990',
        'priceHtml': '17 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3902',
          'title': 'Набор косметики к LG Q6',
          'titleHtml': 'Набор косметики к LG Q6',
          'imageUrl': './images/goods/39.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '12',
        'title': 'LG Q6 α, черный',
        'titleHtml': 'LG Q6 α, черный',
        'imageUrl': './images/goods/12.png',
        'model': 'M700',
        'price': '14990',
        'priceHtml': '14 990 руб.',
        'type': 'device',
        'gift': {
          'id': '4001',
          'title': 'Набор косметики к LG Q6 alfa',
          'titleHtml': 'Набор косметики к LG Q6 alfa',
          'imageUrl': './images/goods/40.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '13',
        'title': 'LG Q6 α, золотой',
        'titleHtml': 'LG Q6 α, золотой',
        'imageUrl': './images/goods/13.png',
        'model': 'M700',
        'price': '14990',
        'priceHtml': '14 990 руб.',
        'type': 'device',
        'gift': {
          'id': '4002',
          'title': 'Набор косметики к LG Q6 alfa',
          'titleHtml': 'Набор косметики к LG Q6 alfa',
          'imageUrl': './images/goods/40.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '14',
        'title': 'LG Q6 α, платиновый',
        'titleHtml': 'LG Q6 α, платиновый',
        'imageUrl': './images/goods/14.png',
        'model': 'M700',
        'price': '14990',
        'priceHtml': '14 990 руб.',
        'type': 'device',
        'gift': {
          'id': '4003',
          'title': 'Набор косметики к LG Q6 alfa',
          'titleHtml': 'Набор косметики к LG Q6 alfa',
          'imageUrl': './images/goods/40.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '15',
        'title': 'Чехол-книжка VOIA для LG V30+',
        'titleHtml': 'Чехол-книжка VOIA для LG V30+',
        'imageUrl': './images/goods/15.png',
        'model': 'черный',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '16',
        'title': 'Чехол-книжка VOIA для LG V30+',
        'titleHtml': 'Чехол-книжка VOIA для LG V30+',
        'imageUrl': './images/goods/16.png',
        'model': 'синий',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '17',
        'title': 'Чехол-книжка VOIA для LG V30+',
        'titleHtml': 'Чехол-книжка VOIA для LG V30+',
        'imageUrl': './images/goods/17.png',
        'model': 'фиолетовый',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '18',
        'title': 'Силиконовая накладка VOIA для LG V30+',
        'titleHtml': 'Силиконовая накладка VOIA для LG V30+',
        'imageUrl': './images/goods/18.png',
        'model': 'прозрачный',
        'price': '790',
        'priceHtml': '790 руб.',
        'type': 'accessory'
      },
      {
        'id': '19',
        'title': 'Защитный экран VOIA для LG V30+',
        'titleHtml': 'Защитный экран VOIA для LG V30+',
        'imageUrl': './images/goods/19.png',
        'model': 'FULL3D',
        'price': '1990',
        'priceHtml': '1 990 руб.',
        'type': 'accessory'
      },
      {
        'id': '20',
        'title': 'Умный чехол-книжка VOIA для LG G6',
        'titleHtml': 'Умный чехол-книжка VOIA для LG G6',
        'imageUrl': './images/goods/20.png',
        'model': 'серебро',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '21',
        'title': 'Умный чехол-книжка VOIA для LG G6',
        'titleHtml': 'Умный чехол-книжка VOIA для LG G6',
        'imageUrl': './images/goods/21.png',
        'model': 'черный',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '22',
        'title': 'Умный чехол-книжка VOIA для LG G6',
        'titleHtml': 'Умный чехол-книжка VOIA для LG G6',
        'imageUrl': './images/goods/22.png',
        'model': 'золотой',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '23',
        'title': 'Умный чехол-книжка VOIA для LG G6',
        'titleHtml': 'Умный чехол-книжка VOIA для LG G6',
        'imageUrl': './images/goods/23.png',
        'model': 'розовый',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '24',
        'title': 'Умный чехол-книжка VOIA для для LG Q6',
        'titleHtml': 'Умный чехол-книжка VOIA для для LG Q6',
        'imageUrl': './images/goods/24.png',
        'model': 'черный',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '25',
        'title': 'Умный чехол-книжка VOIA для для LG Q6',
        'titleHtml': 'Умный чехол-книжка VOIA для для LG Q6',
        'imageUrl': './images/goods/25.png',
        'model': 'золотой',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '26',
        'title': 'Умный чехол-книжка VOIA для для LG Q6',
        'titleHtml': 'Умный чехол-книжка VOIA для для LG Q6',
        'imageUrl': './images/goods/26.png',
        'model': 'серебро',
        'price': '1790',
        'priceHtml': '1 790 руб.',
        'type': 'accessory'
      },
      {
        'id': '28',
        'title': 'LG V30+, 128 ГБ, черный',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, черный',
        'imageUrl': './images/goods/3.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '2701',
          'title': 'Телевизор LG 32LJ500U',
          'titleHtml': 'Телевизор LG 32LJ500U',
          'imageUrl': './images/goods/27.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '29',
        'title': 'LG V30+, 128 ГБ, синий',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, синий',
        'imageUrl': './images/goods/4.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '2702',
          'title': 'Телевизор LG 32LJ500U',
          'titleHtml': 'Телевизор LG 32LJ500U',
          'imageUrl': './images/goods/27.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '30',
        'title': 'LG V30+, 128 ГБ, фиолетовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, фиолетовый',
        'imageUrl': './images/goods/5.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '2703',
          'title': 'Телевизор LG 32LJ500U',
          'titleHtml': 'Телевизор LG 32LJ500U',
          'imageUrl': './images/goods/27.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '31',
        'title': 'LG V30+, 128 ГБ, розовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, розовый',
        'imageUrl': './images/goods/6.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '2704',
          'title': 'Телевизор LG 32LJ500U',
          'titleHtml': 'Телевизор LG 32LJ500U',
          'imageUrl': './images/goods/27.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '33',
        'title': 'LG V30+, 128 ГБ, черный',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, черный',
        'imageUrl': './images/goods/3.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3201',
          'title': 'Набор косметики к LG V30+',
          'titleHtml': 'Набор косметики к LG V30+',
          'imageUrl': './images/goods/32.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '34',
        'title': 'LG V30+, 128 ГБ, синий',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, синий',
        'imageUrl': './images/goods/4.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3202',
          'title': 'Набор косметики к LG V30+',
          'titleHtml': 'Набор косметики к LG V30+',
          'imageUrl': './images/goods/32.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '35',
        'title': 'LG V30+, 128 ГБ, фиолетовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, фиолетовый',
        'imageUrl': './images/goods/5.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3203',
          'title': 'Набор косметики к LG V30+',
          'titleHtml': 'Набор косметики к LG V30+',
          'imageUrl': './images/goods/32.png',
          'isGift': true,
          'type': 'gift'
        }
      },
      {
        'id': '36',
        'title': 'LG V30+, 128 ГБ, розовый',
        'titleHtml': 'LG V30<sup class=\'basket-table-description__name-sup\'>+</sup>, 128 ГБ, розовый',
        'imageUrl': './images/goods/6.png',
        'model': 'H930DS',
        'price': '59990',
        'priceHtml': '59 990 руб.',
        'type': 'device',
        'gift': {
          'id': '3204',
          'title': 'Набор косметики к LG V30+',
          'titleHtml': 'Набор косметики к LG V30+',
          'imageUrl': './images/goods/32.png',
          'isGift': true,
          'type': 'gift'
        }
      },
    ];

    const renderBasketItems = function( id, onlyGift ) {
      var goodsArr = [];

      let choosedItem = itemsData.find( ( item ) => {
        return item.id === id;
      });

      if ( !onlyGift ) {
        goodsArr.push( choosedItem );
      }

      if ( choosedItem.gift ) {
        goodsArr.push( choosedItem.gift );
      }

      var basketItems = '';
      goodsArr.forEach( function( item ) {
        basketItems += `
          <div class="basket-table__row"
            data-item-id="${item.id}"
            ${item.isGift ? 'data-gift="true"' : 'data-related-gift="'+ ( item.gift ? item.gift.id : null ) +'"'}
            data-device-type="${item.type}">
            <div class="basket-table__cell basket-table-photo">
              <div class="basket-table-photo__wrap">
                ${item.isGift ? '<span class="basket-table-photo__gift-label"></span>' : ''}
                <img class="basket-table-photo__image" src="${item.imageUrl}" alt="Фото «${item.title}»">
              </div>
            </div>

            <div class="basket-table__cell basket-table-description">
              <div class="basket-table-description__row basket-table-description__row--top">
                <h3 class="basket-table-description__name">${item.titleHtml}</h3>

                <div class="basket-table-description__extra basket-table-description__extra--mobile">
                  ${item.isGift ? '<span class="basket-table-description__gift">В КОМПЛЕКТЕ X <span class="js-gift-count">1</span> шт</span>'
                  : '<span class="basket-table-description__model"> '+ item.model +' </span>'}
                </div>

                ${item.isGift ?
                  ''
                  :
                  `<span class="basket-table-description__price js-item-price" data-item-price="${item.price}">${item.priceHtml}</span>`}`;

        ( item.isGift ?
          basketItems += '<span class="basket-table-description__total-price"><span class="js-gift-count">1</span>шт.</span>'
          :
          basketItems += `
                  <div class="item-counter js-item-counter basket-table-description__counter">
                    <a class="item-counter__action js-item-counter-action item-counter__action--decrement" href="javascript:;" data-action="decrement">-</a>

                    <input class="item-counter__field js-item-counter-field" type="text" value="1">

                    <a class="item-counter__action js-item-counter-action item-counter__action--increment" href="javascript:;" data-action="increment">+</a>
                  </div>`
        );

        basketItems += `
            ${item.isGift ?
              ''
              :
              `<span class="basket-table-description__total-price js-item-total-price">${item.priceHtml}</span>`}
              </div>

              <div class="basket-table-description__row basket-table-description__row--bottom">
                <div class="basket-table-description__extra">
                ${item.isGift ? '<span class="basket-table-description__gift">В КОМПЛЕКТЕ X <span class="js-gift-count">1</span> шт</span>'
                : '<span class="basket-table-description__model"> '+ item.model +' </span>'}
                </div>`;

            ( !item.isGift ?
              basketItems += `
                <a
                  class="basket-table-description__action-delete js-basket-action-delete"
                  data-target-id="${item.id}"
                  data-tooltip-text="Вы точно хотите удалить?"
                  href="javascript:;"
                  title="Удалить">
                  Удалить
                </a>`
              :
              '');

          basketItems += `
              </div>
            </div>
          </div>
        `;
      });

      if ( onlyGift ) {
        $( '.js-basket-table' ).append( basketItems );
      } else {
        $( '.js-basket-table' ).prepend( basketItems );
      }
    };

    $( 'body' ).on( 'click', '.js-choose-device', function( e ) {
      e.preventDefault();
      const $this = $( this );

      const deviceId = $this.attr( 'data-device-id' );
      const storedItem = basketState.findItem( deviceId );
      const $basketTable = $( '.js-basket-table' );

      // create or increment device
      if ( !storedItem ) {
        basketState.setItem( deviceId );
        renderBasketItems( deviceId );

        // add gift
        let $item = $basketTable.find( '[data-item-id="'+ deviceId +'"]' );
        let relatedGiftId = $item.attr( 'data-related-gift' );

        if ( relatedGiftId && relatedGiftId !== 'null' ) {
          basketState.setItem( relatedGiftId, true );
        }

        // set device type
        basketState.setType( deviceId, $item.attr( 'data-device-type' ) );

        $basketTable.basketConversion();
      } else {
        let $item = $basketTable.find( '[data-item-id="'+ deviceId +'"]' );
        let $counter = $item.find( '.js-item-counter-field' );
        let $counterValue = $counter.val() * 1;

        // gift
        let relatedGiftId = $item.attr( 'data-related-gift' );
        let $giftItem = $basketTable
          .find( '[data-item-id="'+ relatedGiftId +'"]' );

        if ( ! $giftItem.length ) {
          renderBasketItems( deviceId, true );
        }

        $counter.val( ++$counterValue ).trigger( 'change' );
      }

      let target = $this.attr( 'data-target' );
      let $target = $( target );
      let to = $target.offset().top;

      $().scrollTo( to );

      let $trigger = $target.find( '.js-collapse-trigger' );
      if ( ! $trigger.hasClass( 'active' ) ) {
        $target.find( '.js-collapse-trigger' ).trigger( 'click' );
      }
    });
  })();

  // tooltip
  (function() {
    $.fn.tooltip = function( actionType ) {
      const $tooltip = $( '.js-tooltip' );
      const tooltipActionsHash = {
        close: $tooltip.find( '[data-action="close"]' ),
        deleteItem: $tooltip.find( '[data-action="deleteItem"]' )
      };

      const deleteItem = function() {
        $tooltip.removeClass( 'active' );

        let itemId = tooltipActionsHash.deleteItem.attr( 'data-target-id' );
        let $item = $( `[data-item-id="${itemId}"]` );
        let relatedGiftId = $item.attr( 'data-related-gift' );
        let $giftItem = $( `[data-item-id="${relatedGiftId}"]` );

        $item.remove();
        basketState.removeItem( itemId );

        if ( $giftItem.length ) {
          $giftItem.remove();
          basketState.removeItem( relatedGiftId );
        }

        $( '.js-basket-table' ).basketConversion();
      };

      switch ( actionType ) {
        case 'setItemId':
          let id = this.attr( 'data-target-id' );
          tooltipActionsHash.deleteItem.attr( 'data-target-id', id );
          break;

        case 'open':
          let position = this.offset();
          let text = this.attr( 'data-tooltip-text' );

          $tooltip.find( '.js-tooltip-text' ).text( text );
          let tooltipMargin = ~( $tooltip.height() + 8 );

          $tooltip.css({
            'top': position.top,
            'left': position.left,
            'margin-top': tooltipMargin + 'px'
          });

          $tooltip.addClass( 'active' );
          break;

        case 'close':
          $tooltip.removeClass( 'active' );
          break;

        case 'status':
          return $tooltip.hasClass( 'active' ) ? 'open' : 'close';

        case 'deleteItem':
          deleteItem();
          break;

        default:
      }

      return this;
    };

    $( 'body' ).on( 'click', function( e ) {
      const $this = $( this );
      let $target = $( e.target );
      let condition = $().tooltip('status') === 'open' &&
                      !$target.closest( '.js-tooltip' ).length;

      if ( condition ) {
        $this.tooltip( 'close' );
      }
    });

    $( '#js-collapse-basket' )
      .on( 'click', '.js-basket-action-delete', function( e ) {
        e.preventDefault();
        e.stopPropagation();

        let $this = $( this );

        $this.tooltip( 'setItemId' );
        $this.tooltip( 'open' );
    });

    $( '.js-tooltip-action' ).on( 'click', function( e ) {
      e.preventDefault();

      const $this = $( this );
      const actionType = $this.attr( 'data-action' );
      $this.tooltip( actionType );
    });
  })();

  // basket conversion
  (function() {
    $.fn.basketConversion = function() {
      var resultNDS = 0;
      var resultPrice = 0;
      var resultCount = 0;

      const $that = $( this );
      const $rows = $that.find( '[data-item-id]' ).not( '[data-gift="true"]' );

      function numberWithSpaces( x ) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
      }

      $rows.each( function() {
        let $this = $( this );

        let $price = $this.find( '.js-item-price' );
        let price = parseInt( $price.attr( 'data-item-price' ), 10 );

        let $count = $this.find( '.js-item-counter-field' );
        let count = $count.val() * 1;

        let $totalPrice = $this.find( '.js-item-total-price' );
        let totalPrice = numberWithSpaces( price * count );

        resultCount += count;
        resultPrice += price * count;
        $totalPrice.text( totalPrice + ' руб.' );

        let giftId = $this.attr( 'data-related-gift' );
        let $giftItem = $that.find( '[data-item-id="'+ giftId +'"]' );

        if ( $giftItem.length ) {
          $giftItem.find( '.js-gift-count' ).text( count );
        }
      });

      resultNDS = numberWithSpaces( ( resultPrice / 1180 * 180 ).toFixed( 2 ) );
      resultPrice = numberWithSpaces( resultPrice );
      $( '.js-basket-result-nds' ).text( resultNDS  + ' руб.' );
      $( '.js-basket-result-price' ).text( resultPrice + ' руб.' );
    };
  })();

  // open block
  (function() {
    $( '.js-open-block' ).on( 'click', function( e ) {
      e.preventDefault();

      let target = $( this ).attr( 'data-block' );
      let $target = $( target );
      let to = $target.offset().top;

      $().scrollTo( to );

      let $trigger = $target.find( '.js-collapse-trigger' );
      if ( ! $trigger.hasClass( 'active' ) ) {
        $target.find( '.js-collapse-trigger' ).trigger( 'click' );
      }
    });
  })();

  // counter
  (function() {
    $( '#js-collapse-basket' )
      .on( 'click', '.js-item-counter-action', function( e ) {
        e.preventDefault();

        const $this = $( this );
        const $container = $this.closest( '.js-item-counter' );
        const $field = $container.find( '.js-item-counter-field' );
        const $item = $container.closest( '[data-item-id]' );
        const itemId = $item.attr( 'data-item-id' );
        const giftId = $item.attr( 'data-related-gift' );
        const actionType = $this.attr( 'data-action' );
        var value = $field.val() * 1;

        if ( actionType === 'increment' && value < 99 ) {
          value++;
        } else if ( actionType === 'decrement' && value > 1 ) {
          value--;
        }

        $field.val( value );
        basketState.setCount( itemId, value );
        basketState.setCount( giftId, value );

        $( '.js-basket-table' ).basketConversion();
    })
    .on( 'keydown', '.js-item-counter-field', function( e ) {
      if ( $.inArray( e.keyCode, [173, 46, 8, 9, 27, 13, 110, 190] ) !== -1 ||
      ( e.keyCode === 65 && e.ctrlKey ) ||
      ( e.keyCode === 67 && e.ctrlKey ) ||
      ( e.keyCode === 88 && e.ctrlKey ) ||
      ( e.keyCode >= 35 && e.keyCode <= 39) ) {
        return;
      }

      if ( ( e.shiftKey || (e.keyCode < 48 || e.keyCode > 57 )) && ( e.keyCode < 96 || e.keyCode > 105 ) ) {
        e.preventDefault();
      }
    })
    .on( 'change', '.js-item-counter-field', function( e ) {
      const $this = $( this );
      const $item = $this.closest( '[data-item-id]' );
      const itemId = $item.attr( 'data-item-id' );
      const giftId = $item.attr( 'data-related-gift' );
      var value = $this.val() * 1;

      if ( !value ) {
        value = 1;
      } else if ( value > 99 ) {
        value = 99;
      }

      $this.val( value );
      basketState.setCount( itemId, value );
      basketState.setCount( giftId, value );

      $( '.js-basket-table' ).basketConversion();
    });
  })();

  // validation
  (function() {
    $( '.js-customer-form' ).validate({
        rules: {
          cfFullName: {
            required: true
          },
          cfEmail: {
            required: true,
            email: true
          },
          cfTel: {
            required: true
          },
          cfNote: {
            minlength: 8
          },
          cfAddressCity: {
            required: true
          },
          cfAddressStreet: {
            required: true
          },
          cfAddressHouse: {
            required: true
          },
          cfPickupAddress: {
            required: true
          },
          cfRules: {
            required: true
          }
        },

        messages: {
          cfRules: 'Выберите, чтобы продолжить',
          cfAddressCity: 'Заполните Ваш адрес',
          cfAddressStreet: 'Заполните Ваш адрес',
          cfAddressHouse: 'Заполните Ваш адрес',
          cfPickupAddress: 'Выберите адрес пункта самовывоза'
        },

        errorClass: 'error-message',
        focusCleanup: false,
        focusInvalid: false,

        errorPlacement: function( error, element ) {
          const $parent = element.closest( '.js-fieldset' );

          if ( ! $parent.find( '.error-message' ).length ) {
            error.appendTo( $parent );
          }
        },

        success: function( undefined, element ) {
          $( element ).addClass( 'has-success' );
        },

        submitHandler: function( form ) {
          var data = {};
          let formData = $( form ).serializeArray();
          let basketItems = basketState.getItems();

          // save data to ls
          let storedData = JSON.stringify( formData );
          localStorage.setItem( 'userData', storedData );

          formData.forEach( function( item ) {
            data[ item.name ] = item.value;
          });

          // can be use 'beforePaid' or 'robo';
          data.payment = 'robo';

          // device validation
          const devices = basketItems.filter( function( item ) {
            return item.type === 'device';
          });

          if ( !devices.length ) {
            alert( 'Вы не можете приобрести аксессуар без смартфона.' );

            let to = $( '#block-device' ).offset().top;
            $().scrollTo( to );

            return;
          }

          const vacuumGiftIds = ['101', '102', '103', '104'];
          const tvGiftIds = ['2701', '2702', '2703', '2704'];
          const cosmeticsV30Ids = ['3201', '3202', '3203', '3204'];
          const cosmeticsQ6Ids = ['3801', '3802'];
          const cosmeticsQ6PlusIds = ['3901', '3902'];
          const cosmeticsQ6AlphaIds = ['4001', '4002', '4003'];

          const mergeIds = function( id, item, data ) {
            let dataItem = data.goods.find( function( el ) {
              return el.id === id;
            });

            if ( dataItem ) {
              dataItem.count = dataItem.count + item.count;
            } else {
              data.goods.push( $.extend( item, { id: id } ) );
            }
          };

          basketItems.forEach( function( element ) {
            const item = Object.assign( {}, element );

            if ( !data.goods ) {
              data.goods = [];
            }

            // ids transforms
            if ( vacuumGiftIds.indexOf( item.id ) !== -1 ) {

              mergeIds( '1', item, data );

            } else if ( tvGiftIds.indexOf( item.id ) !== -1 ) {

              mergeIds( '27', item, data );

            } else if ( cosmeticsV30Ids.indexOf( item.id ) !== -1 ) {

              mergeIds( '32', item, data );

            } else if ( cosmeticsQ6Ids.indexOf( item.id ) !== -1 ) {

              mergeIds( '38', item, data );

            } else if ( cosmeticsQ6PlusIds.indexOf( item.id ) !== -1 ) {

              mergeIds( '39', item, data );

            } else if ( cosmeticsQ6AlphaIds.indexOf( item.id ) !== -1 ) {

              mergeIds( '40', item, data );

            } else {
              mergeIds( item.id, item, data );
            }
          });

          if ( basketItems.length ) {
            $.ajax({
              type: 'POST',
              url: locationOrigin + '/api/userorder/order',
              data: {
                order: data
              },

              success: function( resp ) {
                if ( !resp.action ) { return; }

                const $form = $( '.js-payment-form' );
                $form.attr( 'action', resp.action );
                $form.children( '[name="MerchantLogin"]' ).attr( 'value', resp.MerchantLogin );
                $form.children( '[name="OutSum"]' ).attr( 'value', resp.OutSum );
                $form.children( '[name="InvId"]' ).attr( 'value', resp.InvId );
                $form.children( '[name="Receipt"]' ).attr( 'value', resp.Receipt );
                $form.children( '[name="Description"]' ).attr( 'value', resp.Description );
                $form.children( '[name="SignatureValue"]' ).attr( 'value', resp.SignatureValue );
                $form.children( '[name="Email"]' ).attr( 'value', resp.Email );
                $form.children( '[name="Culture"]' ).attr( 'value', resp.Culture );
                $form.children( '[name="Encoding"]' ).attr( 'value', resp.Encoding );

                $form[0].submit();
              },

              error: function( resp ) {
                alert( 'Произошла ошибка, попробуйте еще раз' );
              }
            });
          } else {
            alert( 'Корзина пуста, пожалуйста выберите товар.' );
          }
        },

        highlight: function( element ) {
          $( element )
            .addClass( 'has-error' )
            .removeClass( 'has-success' );
        },

        unhighlight: function( element ) {
          $( element ).removeClass( 'has-error' );
        }
      });

      $.extend( $.validator.messages, {
        required: 'Это обязательное поле',
        email: 'Введите корректный адрес',
        minlength: 'Введите не менее 8 символов'
      });
    })();

    // tabs
    (function() {
      $( '.js-tab-trigger' ).on( 'click', function() {
        const $this = $( this );

        if ( $this.hasClass( 'active' ) ) { return; }

        const $container = $this.closest( '.js-tab-container' );
        const $triggers = $container.find( '.js-tab-trigger' );
        const $items = $container.find( '.js-tab-item' );
        const tabIndex = $this.attr( 'data-tab' );
        const dataItemId = $this.attr( 'data-new-item-id' );

        $container.addClass( 'active' );

        $triggers.removeClass( 'active' );
        $this.addClass( 'active' );

        $items.removeClass( 'active' );
        $items.eq( tabIndex ).addClass( 'active' );

        if ( dataItemId ) {
          const $chooseDeviceTrigger = $container.find( '.js-choose-device' );
          $chooseDeviceTrigger.attr( 'data-device-id', dataItemId );
        }
      });
    })();

    // order number
    (function() {
      const findGetParameter = function( parameterName ) {
          var result = null,
              tmp = [];

          location.search
              .substr(1)
              .split('&')
              .forEach( function( item ) {
                tmp = item.split('=');

                if ( tmp[0] === parameterName ) {
                  result = decodeURIComponent( tmp[1] );
                }
              });
          return result;
      }

      let orderNumber = findGetParameter( 'orderNumber' );

      if ( orderNumber ) {
        $( '.js-order-number' ).text( '№ ' + orderNumber );
      }
    })();

    // open chat
    (function() {
      $( '.js-open-chat' ).on( 'click', function( e ) {
        e.preventDefault();

        jivo_api.open();
      });
    })();

    // apply stored data
    (function() {
      let storedData = localStorage.getItem( 'userData' );

      if ( storedData ) {
        let $form = $( '.js-customer-form' );
        let userData = JSON.parse( storedData );

        userData.forEach( function( item ) {
          let $field = $form.find( '[name="'+ item.name +'"]' );
          $field.val( item.value );
        });
      }
    })();

    // delivery type
    (function() {
      $( '.js-delivery-trigger' ).on( 'change', function( e ) {
        let $this = $( this );
        let target = $this.children( ':selected' ).attr( 'data-target' );
        let $parent = $this.closest( '.js-fieldset' );
        let $content = $parent.find( '.js-delivery-content' );

        $content
          .children()
          .removeClass( 'active' )
          .eq( target )
          .addClass( 'active' );
      });
    })();

    $( '.js-delivery-trigger' ).trigger( 'change' );
});
